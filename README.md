README
======

This is the Virtual Manager VM Player repository

Bitrise Builds
--------------

*All* AppStore/PlayStore releases should be done from Bitrise build system to ensure a consistent build environment

In the event that Bitrise AppStore builds fail with Provisioning Profile or Certificate errors (usually expired) make sure the following is done

1. Login to the [_Apple Developer portal_](https://developer.apple.com/account/ios/certificate/) with the steve.fillingham@virtualmgr.com.au appleid, the password for this is shared in Lastpass

1. Goto the _Certificates_ section and renew the following certificates
   
   - Virtual Mgr PTY LTD

1. then download the resulting certificate onto your Mac, then from Keychain Access program export the private key to a .p12 file with a password

1. Upload the .p12 file to the Bitrise _Virtual Mgr_ application under the _Code Signing_ tab and the _CODE SIGNING IDENTITY_ pane ![](/readme_images/BitriseCertificates.png)
and enter the password

1. Goto the _Provisioning Profile_ section of the Apple Developer portal and regenerate the following profiles and then download them

   - Virtual Mgr * Dev Profile
   - Virtual Mgr AdHoc Profile
   - Virtual Mgr AppStore Distribution Profile

1. On the same _Code Signing_ tab on Bitrise upload the above profiles to the _Provisioning Profile_ pane ![](/readme_images/BitriseProvisioningProfiles.png)

1. Delete any expired Profiles and Certificate from the Bitrise panes

1. The AppStore build should now finish without error