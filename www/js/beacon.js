angular.module('vmBeacons', ['vmngUtils'])

.factory('iOSBeaconSvc', ['ngTimeout', 'actionQueue', '$log', '$timeout', 'CRC16', 'binUtils',
    function(ngTimeout, actionQueue, $log, $timeout, CRC16, binUtils) {
    var base64 = cordova.require('cordova/base64');

    var VirtualMgr201609 = "VirtualMgr201609";
    var VirtualMgr201609_Array = new Uint8Array([0x56, 0x69, 0x72, 0x74, 0x75, 0x61, 0x6c, 0x4d, 0x67, 0x72, 0x32, 0x30, 0x31, 0x36, 0x30, 0x39]);
    var VirtualMgr201609_UUID = '56697274-7561-6C4D-6772-323031363039';

    var Eddystone_UUID = 'FEAA';

    // Note - at the moment we cannot use the VirtualManagerBLE "Client" class as it holds onto Peripherals
    // Our beacon handling code below assumes each Peripheral advertisement is a newly instantiated JS object
    var _bts = cordova.plugins.VirtualManagerBLE; 

    // On start-up Stop Native code from scanning - it is possible that page refresh/logout etc could leave Native
    // code scanning in background when new page JS has not requested scan, Cordova will throw away unrecognised Native callbacks
    // but still leaves high CPU usage for no gain
    _bts.stopScanning();

    var q = actionQueue();

    var makeAccuracyFn = function (C, A, k) {
        return function (rssi) {
            var d = C + A * (1 - Math.exp(-k * rssi));
            if (d < 0.1) {
                d = 0.1;
            }
            d = Math.round(d * 100.0) / 100.0;
            return d;
        }
    }

    var uuidHandler = {}
    uuidHandler[Eddystone_UUID] = function () {
        var _kontactRssiToDistanceTable = {
            "4":   makeAccuracyFn( 0.009625294114018431, -0.0010061934872146057,    0.13392619006511164),
            "0":   makeAccuracyFn(-0.017591084765563803, -0.0025113916607353427,    0.11355818870988318),
            "-4":  makeAccuracyFn( 0.7686512749022019,   -0.0000027703393484872667, 0.20078007950670046),
            "-8":  makeAccuracyFn(-0.1290292299955053,   -0.004681297300901694,     0.09164734302123372),
            "-12": makeAccuracyFn(-0.5228184555926533,   -0.00810275302902605,      0.08241227291322066),
            "-16": makeAccuracyFn(-0.8377064486014859,   -0.008758131385488995,     0.07869184330473201),
            "-20": makeAccuracyFn(-1.843414964364145,    -0.01763994612784343,      0.06970931049388987),
            "-30": makeAccuracyFn(-5.9354193589836965,   -1.6706025454598172,       0.019285621365600077)
        }
        // We use a Cache to be able to link different packets from the same Beacon (Eddystone TLM, UID for example)
        var _beaconCache = Object.create({});

        var FT_UID = 0x0;
        var FT_URL = 0x1;
        var FT_TLM = 0x2;
        var FT_EID = 0x3;
        
        var TLM_Unencrypted = 0x00;
        var TLM_Encrypted   = 0x01;

        var URLSchemes = ['http://www.', 'https://www.', 'http://', 'https://'];
        var ShortCuts = {
            0: '.com/',
            1: '.org/',
            2: '.edu/',
            3: '.net/',
            4: '.info/',
            5: '.biz/',
            6: '.gov/',
            7: '.com',
            8: '.org',
            9: '.edu',
            10: '.net',
            11: '.info',
            12: '.biz',
            13: '.gov',
        }

        var utf8decoder = new TextDecoder();

        return function (peripheral) {
            var rssiToDistanceTable = null;
            var kontaktData = peripheral.advertisement.serviceData['D00D'];
            var data = peripheral.advertisement.serviceData['FEAA'];
            var frameType = data[0] >> 4;

            var newdata = {};

            if (kontaktData) {
                // Kontakt D00D data appears to be
                // nnnnVVb
                // where nnnn is the Kontakt name of the beacon (usually printed on beacon)
                // VV is the version of firmware (41 => 4.1)
                // b is the Battery Percent
                rssiToDistanceTable = _kontactRssiToDistanceTable;
                var kid = utf8decoder.decode(kontaktData);
                newdata.beaconId = kid.substr(0, 4);
                newdata.battery = kontaktData[kontaktData.length - 1];      // Last byte is Battery %
            }
            switch (frameType) {
                case FT_UID:
                    newdata.txAt0m = binUtils.bigEndian.readInt8(data, 1);
                    if (!kontaktData) {
                        newdata.beaconId = binUtils.bytesToHex(data, 2, 2 + 10 + 6);
                    }

                    break;

                case FT_URL:
                    newdata.txAt0m = binUtils.bigEndian.readInt8(data, 1);
                    newdata.URL = URLSchemes[data[2]];
                    for (var i = 3 ; i < data.length ; i++) {
                        var shortCut = ShortCuts[data[i]];
                        if (angular.isDefined(shortCut)) {
                            newdata.URL += shortCut;
                        } else {
                            newdata.URL += String.fromCharCode(data[i]);
                        }
                    }
                    break;

                case FT_TLM:
                    var version = data[1];
                    switch (version) {
                        case TLM_Unencrypted:
                            var volts = binUtils.bigEndian.readUInt16(data, 2) / 1000.0;               // Volts
                            if (angular.isUndefined(newdata.battery)) {
                                newdata.battery = (volts - 2.0) * 100;                                           // Kontakt appears to measure 3v in percentage by -2 (ie 2.7v == 70%)
                            }
                            newdata.batteryV = volts;
                            newdata.temperature = binUtils.bigEndian.readUInt16(data, 4) / 0x100;    // Celcius
                            newdata.advertisingCount = binUtils.bigEndian.readUInt32(data, 6);
                            newdata.uptime = binUtils.bigEndian.readUInt32(data, 10) / 10.0;         // Seconds
                            break;
                    }
                    break;

                case FT_EID:
                    break;
            }

            var cachedata = _beaconCache[peripheral.id];
            if (angular.isDefined(cachedata)) {
                angular.extend(cachedata, newdata);
            } else {
                _beaconCache[peripheral.id] = newdata;

                // When the beacon expires (has not been heard for a while) remove our cache data so next time
                // we see the beacon we do not associate with old data
                newdata.expiry = function () {
                    delete _beaconCache[peripheral.id];
                }

                cachedata = newdata;
            }

            angular.extend(peripheral, cachedata);

            if (rssiToDistanceTable) {
                var fn = rssiToDistanceTable[peripheral.advertisement.txPower.toString()];
                if (fn) {
                    peripheral.instantDistance = fn(peripheral.rssi);
                    peripheral.instantTime = moment.utc();
                }
            }

            if (peripheral.beaconId) {
                return peripheral;
            }
        }
    }();

    var makeBeaconId = function (i32) {
        var h = (i32 >>> 0).toString(16);
        return ("00000000" + h).substr(h.length);
    }

    uuidHandler[VirtualMgr201609_UUID] = function () {
        return function (peripheral) {
            var data = peripheral.advertisement.data;
            peripheral.beaconId = makeBeaconId(binUtils.bigEndian.readUInt32(data, 0));
            peripheral.vibrationCount = data[6];
            peripheral.battery = data[7];
            return peripheral;
        };
    }();

    uuidHandler['766D'] = function () {
        var rssiToDistanceFn = makeAccuracyFn(-3.931708, -(0.02757266 / 0.02973224), 0.02973224);

        return function (peripheral) {
            var data = peripheral.advertisement.data;
            var expectedCRC = binUtils.bigEndian.readUInt16(data, 0);
            var calcCRC = CRC16(data, 2, data.length - 2);

            if (expectedCRC != calcCRC) {
                return null;
            }

            peripheral.beaconId = makeBeaconId(binUtils.bigEndian.readUInt32(data, 2));
            var bpraw = binUtils.bigEndian.readInt24(data, 6);
            if (bpraw != 0) {
                peripheral.barometricPressure = bpraw / 100.0;
            }

            var idx = 9;
            while (data[idx] != 0 && idx < 22) {
                var code = data[idx++];
                switch (code) {
                    case 1: // Battery
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        peripheral.battery = (code - 1) * 100 / 5;
                        break;
                    case 7:
                        peripheral.vibrationCount = binUtils.bigEndian.readUInt16(data, idx);
                        idx += 2;
                        break;
                    case 8: // Temperature
                        var raw = binUtils.bigEndian.readUInt8(data, idx);
                        peripheral.temperature = (raw - 0x80) / 2.0;
                        idx++;
                        break;
                    case 9:
                        peripheral.buttonCount = binUtils.bigEndian.readUInt16(data, idx);
                        idx += 2;
                        break;
                }
            }

            peripheral.instantDistance = rssiToDistanceFn(peripheral.rssi);
            peripheral.instantTime = moment.utc();

            return peripheral;
        };
    }();

    var scanningOptions = {
        serviceUUIDs: [Eddystone_UUID]
    };
    
    var _rescanTimer = null;
    
    var svc = {
        configure: function(options) {
       		scanningOptions = options.iOS;
        },
        
        startScanning: function (success, failure) {
            _rescanTimer = (_bts.supports && _bts.supports.rescanTimeout) ? 'not required' : 'started';                                                                                    
            q(function(next) {
                _bts.startScanning(scanningOptions.serviceUUIDs, {
                    allowDuplicate: true,
                    groupTimeout: 500,
                    groupSize: 0,
                    rescanTimeout: 30000,

                    scanMode: 2,                // Android - see SCAN_MODE_BALANCED
                    matchMode: 1,               // Android - see MATCH_MODE_AGGRESSIVE
                    callbackType: 1,            // Android - see CALLBACK_TYPE_ALL_MATCHES
                    numOfMatches: 1             // Android - see MATCH_NUM_MAX_ADVERTISEMENT

                }, ngTimeout(function (peripherals) {
                    // Issue a re-scan every 30 seconds, iOS for some reason slows scan rate down over time
                    if (_rescanTimer == 'started') {
                        _rescanTimer = $timeout(function() {
                        	if (_rescanTimer != 'stopped') {
	                            _rescanTimer = 'started';
    	                        svc.startScanning(success, failure);
                            }
                        }, 30000);
                    }
                    
                    if (peripherals && success) {
                        var beacons = [];
                        angular.forEach(peripherals, function(peripheral) {                            
                            if (peripheral.rssi < 0 && peripheral.advertisement) {
                                var pa = peripheral.advertisement;
                                if (pa.data) { 
                                    pa.data = new Uint8Array(base64.toArrayBuffer(pa.data));
                                }
                                if (pa.serviceData) {
                                    angular.forEach(pa.serviceData, function (b64, uuid) {
                                        pa.serviceData[uuid] = new Uint8Array(base64.toArrayBuffer(b64));
                                    });
                                }

                                var pushed = false;
                                angular.forEach(pa.UUIDS, function (uuid) {
                                    var h = uuidHandler[uuid];
                                    if (angular.isDefined(h)) {
                                        try {
                                            var p = h(peripheral);
                                            if (p && !pushed) { 
                                                beacons.push(p);
                                                pushed = true;
                                            }
                                        } catch (e) {
                                            // Ignore the error, not much we can do with it anyway
                                        }
                                    }
                                });
                            }
                        });
                        if (beacons.length) {
	                    	success(beacons);
                        }
                    }
                }, next), ngTimeout($log.error, failure, next));
            });
        },

        stopScanning: function(success, failure) {
            q(function(next) {
                if (_rescanTimer && _rescanTimer.$state) {
                    $timeout.cancel(_rescanTimer);
                    _rescanTimer = 'stopped';
                }
                
                _bts.stopScanning(
                    ngTimeout(success, next), 
                    ngTimeout($log.error, failure, next));
            });
        },

        getServices: function(peripheral, success, failure) {
            q(function(next) {
                _bts.peripheral_getServices(peripheral.id, ngTimeout(function(services) {
                    peripheral.services = services;
                    if (success) {
                        success(services);
                    }
                }, next), ngTimeout(failure, next));
            });
        }
    }

    q(function(next) {
        _bts.subscribeStateChange(function(state) {
            if (state == 'PoweredOn') {
                next();
            }
        });
    });

    return svc;
}])

.factory('platformBeaconSvc', ['$injector', '$log', function ($injector, $log) {
    if (window.device && window.device.platform) {
        if (window.device.platform.toLowerCase() == 'ios') {
            // Early BLE versions did not support multiple Clients but still usable for Beacons
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.VirtualManagerBLE) {
                return $injector.get('iOSBeaconSvc');
            }
        } else if (window.device.platform.toLowerCase() == 'android') {
            // Android 1.6.6 supports BLE where the BLE plugin also supported multiple Clients
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.VirtualManagerBLE && window.cordova.plugins.VirtualManagerBLE.Client) {
                return $injector.get('iOSBeaconSvc');       // Yes Android uses same iOS plugin name
            }   
        }
    }
    return {
        notsupported: true
    }
}])

.factory('beaconRanges', [function () {
    // Simple table of Immediate, Near, Far ranges
    var c = [
        { id: 0, name: 'Immediate', distance: 2.0 },
        { id: 1, name: 'Near', distance: 10.0 },
        { id: 2, name: 'Far' },
        { id: 3, name: 'Gone' }     // Not a real "range" but timeout is used to determine we can no longer see it
    ];
    c.Immediate = c[0];
    c.Near = c[1];
    c.Far = c[2];
    c.Gone = c[3];
    return c;
}])

.factory('beaconSvc', ['platformBeaconSvc', '$timeout', '$log', 'rfc4122', 'beaconRanges', 'cookieTimerSvc', function(pbs, $timeout, $log, rfc4122, beaconRanges, cookieTimerSvc) {
    var _cookieTimer = cookieTimerSvc();
    var _beacons = [];
    var _beaconsById = Object.create(null);

    var _options = {
        averageCount: 10,
        expiredTimeoutSeconds: 60,
        rangeChangeTrigger: 3,              // Number of "instant" range readings to occur before it changes
        ignoreDistance: 15
    };

    function _instantBeaconRange(distance, ranges) {
        if (distance <= ranges[0].distance) {
            return ranges[0];

        } else if (distance <= ranges[1].distance) {
            return ranges[1];

        } else {
            return ranges[2];
        }
    }

	var _subscribers = [];
    
    var _makeSubscribers = function(list, key) {
        return function() {
            var args = Array.prototype.slice.call(arguments);
            list.forEach(function(subscriber) {
                if (subscriber[key]) {
                    subscriber[key].apply(null, args);
                }
            });
        };
    };
    
    function _estimateDistance(beacon) {
        // Distance is based on RSSI, if RSSI is bad then we abort
        if (beacon.rssi == 127) {
            return;
        }
        
        // Average the distance first
        beacon.receiveCount = (beacon.receiveCount || 0) + 1;
        beacon.lastXSeconds = beacon.lastXSeconds || [];
        beacon.lastXSeconds.push({
            distance: beacon.instantDistance,
            timestamp: beacon.instantTime,
            rssi: beacon.rssi
        });

        var averagingFrom = moment.utc().subtract(10, 'seconds');
        beacon.lastXSeconds = beacon.lastXSeconds.exclude(function (r) {
            return r.timestamp.isBefore(averagingFrom);
        });

        beacon.distance = beacon.lastXSeconds.average(function (r) { return r.distance; }) || 20;       // If no average then use 20m (Far)
        beacon.averageRssi = beacon.lastXSeconds.average(function (r) { return r.rssi; });
    }

    function _rangeCheck(beacon, same, further, closerOrNew) {
        beacon.instantRange = _instantBeaconRange(beacon.distance, beaconRanges);
        var firstTime = angular.isUndefined(beacon.range);
        if (firstTime || (beacon.instantRange.id != beacon.range.id)) {
            beacon.range = beacon.instantRange;

            if (angular.isUndefined(beacon.bestRange) || beacon.range.id < beacon.bestRange.id) {
                beacon.bestRange = beacon.range;
                closerOrNew.push(beacon);
            } else {
                further.push(beacon);
            }
        } else {
            same.push(beacon);
        }
        return false;
    }


    // Keep a cache of beacons by beaconId, so if a beacon disappears then reappears we can reallocate the same object
    // this allows external clients to keep identity reference to a beacon 
    var _pastBeacons = Object.create(null);

    var _startScanning = function() {
        pbs.startScanning(function(peripherals) {
			if (!svc.scanning) {
				return;                                                                                                                            
            }                                                                                    
            svc.scanCount += peripherals.length;
            
            var beacons = _beacons.clone();
            var closerOrNew = [];
            var same = [];
            var further = [];
            angular.forEach(peripherals, function (peripheral) {
                if (peripheral.instantDistance < _options.ignoreDistance) {
                    var beacon = beacons.find(function (p) {
                        return p.beaconId == peripheral.beaconId;
                    });

                    if (!beacon) {
                        peripheral.sessionId = rfc4122.v4();

                        var pastBeacon = _pastBeacons[peripheral.beaconId];
                        if (pastBeacon) {
                            angular.extend(pastBeacon, peripheral);
                        } else {
                            pastBeacon = peripheral;
                            _pastBeacons[peripheral.beaconId] = pastBeacon;

                            var thisBeacon = pastBeacon;
                            thisBeacon.resetSession = function (everything) {
                                delete thisBeacon.range;
                                delete thisBeacon.bestRange;
                                delete thisBeacon.instantRange;
                                delete thisBeacon.receiveCount;
                                if (everything) {
                                    delete thisBeacon.firstRangeUtc;
                                    delete thisBeacon.lastXSeconds;
                                    delete thisBeacon.sessionId;
                                    delete thisBeacon.timestamp;
                                }
                            }
                        }

                        beacons.push(pastBeacon);
                        beacon = pastBeacon;
                    } else {
                        angular.extend(beacon, peripheral);
                    }
                    beacon.timestamp = moment.utc();
                    beacon.firstRangeUtc = beacon.firstRangeUtc || beacon.timestamp;
                    _estimateDistance(beacon);
                    _rangeCheck(beacon, same, further, closerOrNew);

                    // Must calculate distance every 10 seconds (otherwise beacons that quickly go out of range will stay Immediate or Near until they expire)
                    _cookieTimer.setTimer(beacon.beaconId + ':distancing', 10 * 1000, beacon, function (beacon, timer) {
                        var closerOrNew = [];
                        var same = [];
                        var further = [];

                        _estimateDistance(beacon);
                        _rangeCheck(beacon, same, further, closerOrNew);

                        if (further.length || closerOrNew.length) {
                            _makeSubscribers(_subscribers, 'changed')({ alive: _beacons, closerOrNew: closerOrNew, same: same, further: further });
                        } else {
                            _makeSubscribers(_subscribers.filter(function (s) {
                                return s.options.anyChange;
                            }), 'changed')({ alive: _beacons });
                        }

                        timer.reset(10 * 1000);
                    });

                    _cookieTimer.setTimer(beacon.beaconId + ':expiry', _options.expiredTimeoutSeconds * 1000, beacon, function (beacon) {
                        _cookieTimer.clearTimer(beacon.beaconId + ':distancing');

                        // Beacon has expired, remove it
                        beacon.range = beaconRanges.Gone;         // Gone

                        // beacon expired, tidy it up
                        if (angular.isFunction(beacon.expired)) {
                            beacon.expired();
                        }

                        var index = _beacons.indexOf(beacon);
                        _beacons.splice(index, 1);

                        _makeSubscribers(_subscribers, 'changed')({ expired: [beacon], alive: _beacons });

                        beacon.resetSession(true);
                    });
                }
            });

            _beacons.splice(0, _beacons.length);
            _beacons.push.apply(_beacons, beacons);
            _beacons.sort(function(a,b) { 
                return a.beaconId.localeCompare(b.beaconId);
            });

            if (further.length || closerOrNew.length) {
                _makeSubscribers(_subscribers, 'changed')({ alive: _beacons, closerOrNew: closerOrNew, same: same, further: further });
            } else {
                _makeSubscribers(_subscribers.filter(function(s) {
					return s.options.anyChange;
                }), 'changed')({ alive: _beacons });
			}
        });
    }
        
    var svc = {
        scanning: false,
        
        scanCount: 0,
        
        getBeacons: function() {
            return _beacons;
        },

        // getPastBeacon - we keep a list _pastBeacons of *all* beacons we have seen even after they "expire"
        // this allows us to attach semi-persistant data to a beacon which lives on through 1 or more expiries
        // such as "is this a Static or Asset Beacon?"
        getPastBeacon: function (id) {
            return _pastBeacons[id];
        },

        configure: function(options) {
            if (pbs.notsupported)
                return;

            _options = angular.extend(_options, options.general);
            
            pbs.configure(options);    

            if (svc.scanning) {
                _startScanning();
            }
        },
        
        startScanning: function(changed, options) {
            if (pbs.notsupported)
                return;

			options = options || {
            	forceScan: true                                                                                                             
            };
                                                                                    
			var subscription = {
                options: options,                                                                                              
                changed: changed
            };
            _subscribers.push(subscription);

            if (!svc.scanning && options.forceScan) {
                svc.scanning = true;
                
                _startScanning();
            }
            
            // Calling the returned function will unsubscribe and stopScanning if last subscriber
            return function() {
                _subscribers.remove(function(i) {
                    return i === subscription;
                });

                if (_subscribers.count(function(s) { 
                	return s.options.forceScan == true;
                }) == 0) {
		            svc.scanning = false;
        		    pbs.stopScanning();

		            _beacons.splice(0, _beacons.length);
                }
            }
        }
    }
    
    return svc;
}])

