angular.module('vmngUtils', [])

.factory('ngTimeout', ['$timeout', function ($timeout) {
    var _insideAngular = 0;

    return function ngTimeout() {
        var fns = Array.prototype.slice.call(arguments);
        var empty = true;
        fns.forEach(function (fn) {
            if (angular.isFunction(fn)) {
                empty = false;
            }
        });

        if (empty) {
            return angular.identity;
        }

        return function () {
            var args = Array.prototype.slice.call(arguments);
            var callback = function () {
                _insideAngular++;
                try {
                    fns.forEach(function (fn) {
                        if (angular.isFunction(fn)) {
                            fn.apply(null, args);
                        }
                    });
                } finally {
                    _insideAngular--;
                }
            };

            if (_insideAngular == 0) {
                $timeout(callback, 0);
            } else {
                // Already within Angular, call direct
                callback();
            }
        };
    };
}])

.factory('actionQueue', [function () {
    var _nextId = 0;
    return function () {
        var f = function (action) {
            if (f._q) {
                f._q.push(action);
            } else {
                f._q = [];

                var firstTime = true;
                var finish = function () {
                    if (!firstTime) {
                        return;
                    }
                    firstTime = false;

                    if (f._q && f._q.length) {
                        var next = f._q.shift();
                        next(finish);
                    }
                    if (f._q && f._q.length == 0) {
                        f._q = null;
                    }
                }

                action(finish);
            }
        };

        f._id = ++_nextId;
        f._q = null;
        return f;
    };
}])
