/* This is the Cordova part of things for the index page */
var dashboardApp = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    getSettings: function(fn) {
        window.persistantStorage.getItem('settings', function(settings) {
            if(settings == null) {
                fn({});
            }

        	fn(JSON.parse(settings));
        });
    },
    requestFileSystem: function(callback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024*1024*10, function(fs) {
            dashboardApp.fs = fs;
            callback();

        }, function(error) {
            dashboardApp.fs = null;
            console.log('File system request failed');
            callback();
        });  
    },
    jsBundleUrl: function(fullurl) {
        if(dashboardApp.fs == null || dashboardApp.fs === undefined) {
            // let's use the live URL (this should only really happen in testing in a browser)
            return fullurl + '/api/v1/assets/jsbundle?id=jsbundle_ios';
        }
        else {
            return httpDaemon.url + "bundle.js";
        }
    },
    cssBundleUrl: function(fullurl) {
        if(dashboardApp.fs == null || dashboardApp.fs === undefined) {
            // let's use the live URL (this should only really happen in testing in a browser)
            return fullurl + '/api/v1/assets/cssbundle?id=cssbundle_ios';
        }
        else {
            return httpDaemon.url + "bundle.css";
        }
    },
    getTemplateBundle: function(fullUrl, fn) {
        if (dashboardApp.fs == null || dashboardApp.fs === undefined) {
            $.get(fullUrl + '/api/v1/assets/templatebundle', fn);
        } else {
			window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dirEntry) {
                dirEntry.getFile('bundle.html', {}, function(fileEntry) {
                    // Get a File object representing the file,
                    // then use FileReader to read its contents.
                    fileEntry.file(function(file) {
                       var reader = new FileReader();

                       reader.onloadend = function(e) {
                           fn(this.result);
                       };

                       reader.readAsText(file);
                    }, function(error) {
                        log.console('File Read error ' + error);
                    });
                }, function(error) {
                    log.console('File Open error ' + error);
                });
            }, function(error) {
                log.console('Directory Open error ' + error);
            });
        }
    },
    templateBundleUrl: function(fullurl) {
        if(dashboardApp.fs == null || dashboardApp.fs === undefined) {
            // let's use the live URL (this should only really happen in testing in a browser)
            return fullurl + '/api/v1/assets/templatebundle';
        }
        else {
            return httpDaemon.url + "bundle.html";
        }
    },
    createCss: function(url, elt) {
       var css = document.createElement('link');
       css.rel = "stylesheet";
       css.href = url;

       $(elt).append(css);        
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        httpDaemon.onDeviceReady();
        
        dashboardApp.receivedEvent('deviceready');
        navigator.splashscreen.hide();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        httpDaemon.start(function() {
            dashboardApp.requestFileSystem(function() {
           		var body = document.querySelector('body');
                
                var settings = dashboardApp.getSettings(function(settings) {
                    
                    window.razordata = {
                        deleteurl: '',
                        apiprefix: settings.fullUrl + '/api/v1/',
                        siteprefix: settings.fullUrl,
                        environment: 'mobile'
                    };
                    
                    if(window.razordata.siteprefix.substr(-1) != '/') {
                        window.razordata.siteprefix = window.razordata.siteprefix + '/';
                    }

                    // Note, bundle.css is now loaded directly from the page
                    
                    dashboardApp.getTemplateBundle(settings.fullUrl, function(template) {
                        template = $(template);
                        $('#htmlTemplates').html(template);
                        
                        var head = document.getElementsByTagName('head')[0];
                        var js = document.createElement('script');
                        js.type = "text/javascript";
                        js.src = dashboardApp.jsBundleUrl(settings.fullUrl);

                        // We rely on bundlejs/runapp.js to actually bootstrap Angular
                        
                        head.appendChild(js);
                    })

                    // Just logged in and Dashboard has just loaded, we can see if this is 1st time we have logged in after the iOS13Fix 
                    // where cookies just stopped working and we had to use JWT and persistantStorage, set a flag so vmAuthenticate can
                    // pick this scenario up and see if a 2nd login is required ..
                    window.iOS13Fix_DashboardLoaded = 1;
                });
            });
        });
    }
};