var vmModule = angular.module('player', ['oc.lazyLoad', 
                                                 'ui.router',
                                                 'ui.router.router',
                                                 'ui.router.state',
                                                 'ConsoleLogger',
                                                 'snap',
                                                 'uuid',
                                                 'vmBeacons',
                                                 'bc.AngularKeypad',
                                                 'ngToast'
                                                 ]);

angular.tryFromJson = function(json) {
    try {
        return angular.fromJson(json);
    } catch (ex) {
        return null;
    }
}

vmModule.config(['ngToastProvider', function (ngToast) {
    ngToast.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        maxNumber: 3
    });
}]);

vmModule.factory('iOS13CookieFixHttpInterceptor', ['$injector', function ($injector) {
    var _authSvc;

    return {
        request: function (config) {
            _authSvc = _authSvc || ($injector.has('vmAuthenticate') ? $injector.get('vmAuthenticate') : null);

            if (_authSvc) {
                return _authSvc.getUser().then(function (user) {
                    if (user.token) {
                        config.headers['Authorization'] = 'Bearer ' + user.token;
                    }
                    return config;
                });
            }

            return config;
        }
    }
}])

vmModule.config(
    ['$ocLazyLoadProvider', '$provide', '$httpProvider',
     function($ocLazyLoadProvider, $provide, $httpProvider) {
         $httpProvider.defaults.withCredentials = true;
         $httpProvider.interceptors.push('iOS13CookieFixHttpInterceptor');

// Comment this in if you want to see events from lazy loader in the console
         /*     	$ocLazyLoadProvider.config({
          debug: true
        }); */
         
        $provide.decorator( '$log', [ "$delegate", function( $delegate ) {
			var debugFn = $delegate.debug;
            var logFn = $delegate.log;
            var infoFn = $delegate.info;
            var warnFn = $delegate.warn;
            var errorFn = $delegate.error;
            
            $delegate.messages = [];
            
            $delegate._addMessage = function (level, message) {
                var dt = new Date();
                message = dt.toTimeString() + ' - ' + message;
                $delegate.messages.push({ level: level, message: message });
                return message;
            }
            
/*          Not going to decorate .debug - this is for Safari debugging only 
			$delegate.debug = function() {
                $delegate._addMessage('debug', arguments[0]);
                
                debugFn.apply(null, arguments);
            }
*/
            $delegate.log = function () {
                arguments[0] = $delegate._addMessage('log', arguments[0]);
                
                logFn.apply(null, arguments);
            }
            $delegate.info = function() {
                arguments[0] = $delegate._addMessage('info', arguments[0]);
                
                infoFn.apply(null, arguments);
            }
            $delegate.warn = function() {
                arguments[0] = $delegate._addMessage('warn', arguments[0]);
                
                warnFn.apply(null, arguments);
            }
            $delegate.error = function() {
                arguments[0] = $delegate._addMessage('error', arguments[0]);
                
                errorFn.apply(null, arguments);
            }
            
            return $delegate;
        }]);
     }]
);

// comment this in if you want to see events from the $routing side of things in the console
/*
vmModule.run(['PrintToConsole', function(PrintToConsole) {
    PrintToConsole.active = true;
}]);
*/

vmModule.factory('persistantStorage', ['$timeout', function($timeout) {
    console.log('ng_persistantStorage from vmApp.js');

    var _onKeyChanged = Object.create(null);
    var rawSvc;
    if (window.persistantStorage) {
        var _ps = window.persistantStorage;
        // Make persistantStorage callbacks Angular aware .. 
        rawSvc = {
            getItem: function (key, success) {
                _ps.getItem(key, success ? function (data) {
                    $timeout(success, 0, true, data);
                } : null);
            },
            setItem: function (key, data, success, failure) {
                _ps.setItem(key, data, success ? function (result) {
                    $timeout(success, 0, true, result);
                } : null, failure ? function (err) {
                    $timeout(failure, 0, true, err);
                } : null);
            },
            removeItem: function (key, success) {
                _ps.removeItem(key, success ? function (exists) {
                    $timeout(success, 0, true, exists);
                } : null);
            },
            clear: function (success) {
                _ps.clear(success ? function () {
                    $timeout(success);
                } : null);
            }
        }
    } else {
        // Revert to localStorage ..
        rawSvc = {
            getItem: function (key, success) {
                var data = localStorage.getItem(key);
                if (success) {
                    $timeout(success, 0, true, data);
                }
            },
            setItem: function (key, data, success, failure) {
                try {
                    localStorage.setItem(key, data);
                    if (success) {
                        $timeout(success, 0, true, data);
                    }
                } catch (err) {
                    if (failure) {
                        $timeout(failure, 0, true, err);
                    }
                }
            },
            removeItem: function (key, success) {
                var exists = localStorage.getItem(key) != null;
                localStorage.removeItem(key);
                if (success) {
                    $timeout(success, 0, true, exists);
                }
            },
            clear: function (success) {
                localStorage.clear();
                if (success) {
                    $timeout(success);
                }
            }
        }
    }
    return angular.extend({}, rawSvc, {
        // We can add the following function which "watches" a key and calls the callback whenever setItem is called
        onChange: function (key, callback) {
            var changeList = _onKeyChanged[key];
            if (angular.isUndefined(changeList)) {
                changeList = [];
                _onKeyChanged[key] = changeList;
            }
            changeList.push(callback);

            // Get the item now ..
            rawSvc.getItem(key, callback);

            // Return unsubscribe function
            return function () {
                var i = changeList.indexOf(callback);
                if (i >= 0) {
                    changeList.splice(i, 1);
                    if (changeList.length == 0) {
                        delete _onKeyChanged[key];
                    }
                }
            }
        },
        setItem: function (key, data, success, failure) {
            return rawSvc.setItem(key, data, function (result) {
                if (success) {
                    success(result);
                }
                var changeList = _onKeyChanged[key];
                if (angular.isDefined(changeList)) {
                    angular.forEach(changeList, function (cb) {
                        cb(data);
                    })
                }
            }, failure);
        },
        removeItem: function (key, success) {
            return rawSvc.removeItem(key, function (exists) {
                if (success) {
                    success(exists);
                }
                var changeList = _onKeyChanged[key];
                if (angular.isDefined(changeList)) {
                    angular.forEach(changeList, function (cb) {
                        cb(null);           // null -> removed
                    })
                }
            })
        }
    });
}]);

// This service is used to manage the local settings on the device (URL etc).
vmModule.factory('vmLocation', ['$log', '$timeout', 'vmSettings', function ($log, $timeout, vmSettings) {
    var vmLocationService = {
        nextUrl: null,
        httpStopped: false,
        reloadCheck: null,
        href: function (url) {
            $log.debug('Looking to navigate to ' + url);
            if (vmLocationService.httpStopped == true) {
                $log.debug('httpstopped, saving url');
                vmLocationService.nextUrl = url;
                return; // we wait until the httpdaemon is running
            }

            // everything is running, so navigate away
            if (url != 'reload') {
                window.location = url;
            }
            else {
                window.location.reload();
            }
        },
        reload: function (url) {
            $log.debug('Looking to reload');
            if (vmLocationService.httpStopped == true) {
                $log.debug('httpstopped, saving reload request');
                vmLocationService.nextUrl = 'reload';
                return; // we wait until the httpdaemon is running
            }

            // before actually reloading things, we hit the server with an Ajax call
            // to test we are online.
            vmLocationService.reloadCheck = 'Checking';
            vmSettings.checkUrl(url).then(function () {
                $log.debug('Ping success, attempting reload');
                window.location.reload();
            }, function () {
                vmLocationService.reloadCheck = 'Failed';
                $log.debug('Ping was a failure, abandoning reload');
            });
        },
        started: function () {
            $timeout(function () {
                vmLocationService.httpStopped = false;

                if (vmLocationService.nextUrl != null) {
                    var l = vmLocationService.nextUrl;
                    vmLocationService.nextUrl = null;

                    vmLocationService.href(l);
                }
            }, 1000); // we put a delay on this to give the HTTP Daemon to start up
        },
        stopped: function () {
            vmLocationService.httpStopped = true;
        }
    }

    document.addEventListener('deviceready', vmLocationService.started, false);
    document.addEventListener("pause", vmLocationService.stopped, false);
    document.addEventListener("resume", vmLocationService.started, false);

    return vmLocationService;
}]);


// This service is used to manage the local settings on the device (URL etc).
vmModule.factory('vmSettings', ['$http', '$q', '$log', 'rfc4122', 'appVersion', 'appDevice', 'persistantStorage', function ($http, $q, $log, newguid, appVersion, appDevice, persistantStorage) {
    var vmSettingsService = {
        _config:null,
        getConfig: function() {
            var deferred = $q.defer();

            if(vmSettingsService._config != null) {
                deferred.resolve(vmSettingsService._config);
            }
            else {
                $http.get("configuration/AppSettings.json").then(function(json) {
                    $log.debug('App Config loaded as ' + JSON.stringify(json.data));
                    
                    vmSettingsService._config = json.data;
                    deferred.resolve(vmSettingsService._config);
                }, function(reason) {
                    $log.error('An error occurred retrieving the config file');
                    deferred.reject(reason);
                });                    
            }
            
			return deferred.promise;
        },
        getSettings: function(success) {
            persistantStorage.getItem('settings', function(settings) {
                if (success) {                
                    success(JSON.parse(settings) || {});
                }
            });
        },
        setSettings: function(settings, success) {
          while (settings.url.endsWith('/')) {
              settings.url = settings.url.substr(0, settings.url.length-1);
          }
          var schemeUrl = settings.url;
          if (!schemeUrl.startsWith('http')) {
              schemeUrl = 'https://' + schemeUrl;
          }
          if (settings.fullUrl == null || settings.fullUrl === undefined) {
              settings.fullUrl = schemeUrl + '/' + vmSettingsService._config.virtualapp;
          }
          var set = JSON.stringify(settings);
          persistantStorage.setItem('settings', set, function() {
            $log.debug('Settings saved as ' + set);
            if (success) {
                success();
            }
          });
        },
        hasSetting: function(property, cb) {
            vmSettingsService.getSettings(function(settings) {
                if(settings[property] == undefined) {
                    $log.debug(property + ' setting not found');
                    cb(false);
                } else {
                    cb(true);
                }
            });            
        },
        checkUrl: function (url) {
            var deferred = $q.defer();
            var uid = newguid.v4();
            
            if (!url.startsWith('http')) {
                url = 'https://' + url;
            }
            while(url.endsWith('/')) {
                url = url.substr(0, url.length-1);
            }

            var fullUrl = url;

            if (fullUrl.endsWith('portal')) {
                fullUrl = fullUrl + '/api/v1/Assets/Echo?request=' + uid;
            }
            else {
                fullUrl = fullUrl + '/portal/api/v1/Assets/Echo?request=' + uid;
            }

            var r = $http({ method: 'GET', url: fullUrl }).then(function (response) {
                if (response.status == 200) {
                    var jobj = angular.tryFromJson(response.data);
                    if (jobj && jobj.Echo == uid) {
                        deferred.resolve(true);
                        return;
                    }
                }
                // error?
                deferred.reject();
            }, function (error) {
                deferred.reject();
            });
            return deferred.promise;
        },
        getProfile: function(cb) {
            persistantStorage.getItem('TabletProfile', function (profile) {
                cb(angular.isString(profile) ? JSON.parse(profile) : profile);
            });
        },
        onProfileChange: function (cb) {
            return persistantStorage.onChange('TabletProfile', cb);
        },
        updateProfile: function (fullurl) {
            var deferred = $q.defer();

            var fnUpdateProfile = function (fullurl) {
                $http({
                    method: 'post',
                    url: fullurl + '/API/v1/App/GetProfileForTablet',
                    data: {
                        TabletUUID: appDevice.uuid
                    }
                }).then(function (response) {
                    persistantStorage.getItem('TabletProfile', function (storedProfile) {

                        var newDesc = angular.extend({}, appVersion, appDevice);

                        // Only update server if we have new "description"
                        function fnUpdateTabletDescription() {
                            persistantStorage.getItem('TabletDescription', function (storedDesc) {
                                storedDesc = storedDesc || {};
                                if (JSON.stringify(storedDesc) != JSON.stringify(newDesc)) {
                                    var timestampedDesc = angular.extend({
                                        TimeStampUtc: new Date().toUTCString()
                                    }, newDesc);

                                    $http({
                                        method: 'post',
                                        url: fullurl + '/API/v1/App/UpdateTablet',
                                        data: {
                                            TabletUUID: appDevice.uuid,
                                            Description: JSON.stringify(timestampedDesc)
                                        }
                                    }).then(function () {
                                        // Ok, now we can save locally
                                        persistantStorage.setItem('TabletDescription', newDesc, function () {
                                            deferred.resolve();
                                        });
                                    }, function (error) {
                                        deferred.reject();
                                    });
                                } else {
                                    // No change in "description"
                                    deferred.resolve();
                                }
                            })
                        }
                        var newProfile = response.data || {};
                        storedProfile = storedProfile || {};
                        if (JSON.stringify(newProfile) != JSON.stringify(storedProfile)) {
                            persistantStorage.setItem('TabletProfile', newProfile, fnUpdateTabletDescription);
                        } else {
                            // Tablet profile has not changed ..
                            fnUpdateTabletDescription();
                        }
                    });
                }, function (error) {
                    deferred.reject(error);
                });
            }

            if (!fullurl) {
                vmSettingsService.getSettings(function (settings) {
                    fnUpdateProfile(settings.fullUrl);
                });
            } else {
                fnUpdateProfile(fullurl);
            }

            return deferred.promise;
        }
    };
    
    return vmSettingsService;
}]);

// Service to handle authentication against the server. The URL handed into it
// should be the URL to the root of the site.
vmModule.factory('vmAuthenticate', ['$http', '$q', '$log', 'persistantStorage', 'appDevice', function ($http, $q, $log, persistantStorage, appDevice) {
	var svc = {
        user: {
            username: '',
            authenticated: false,
            unknown: true
        },
        loggingIn: false,
        getUser: function () {
            return new Promise(function (resolve, reject) {
                if (svc.user.unknown) {
                    svc.lastLogin(function (data) {
                        if (data) {
                            svc.user.username = data.username;
                            svc.user.authenticated = true;
                            svc.user.token = data.token;
                            svc.user.unknown = false;
                            resolve(Object.assign({
                                password: data.password     // Slightly nasty but needed for iOS13Fix (see window.iOS13Fix_DashboardLoaded)
                            }, svc.user));
                        } else {
                            resolve(svc.user);
                        }
                    })
                } else {
                    resolve(svc.user);
                }
            })
        },
        logout: function(success) {
            persistantStorage.removeItem('lastLogin', function() {
                angular.extend(svc.user, {
                    username: '',
                    authenticated: false,
                    token: null
                });
                if (success) {
                    success();
                }
            });
        },
        lastLogin: function(cb) {
            persistantStorage.getItem('lastLogin', function(login) {
                cb(login != null ? JSON.parse(login) : null);
            });
        },        
        login: function(url, username, password) {
            var deferred = $q.defer();
            svc.loggingIn = true;
            
            var cancelled = false;
            var canceller = $q.defer();
            var cancel = function(reason) {
                cancelled = true;
                canceller.resolve(reason || 'Cancelled');
            };

            var clientDateTime = moment.utc().format('YYYY-MM-DDTHH:mm:ss');
            
            $http({
               method: 'post',
               timeout: canceller.promise,
               url: url + '/Authenticate/Login',
               withCredentials: true,
               data: {
                   Username: username,
                   Password: password,
                   ClientDateTime: clientDateTime,
                   IsApiLogin: true,
                   TabletUUID: appDevice.uuid,
                   iOS13Fix: true
               }
            }).then(function(response) {
                svc.loggingIn = false;
                var token;
                // This is our iOS13 fix check where we manually grab the auth token
                if (response.data && response.data.result === true) {
                    token = response.data.token;
                    response.data = 'true';
                }
                if(response.data == 'true') {
                    persistantStorage.setItem('lastLogin', 
                        JSON.stringify({
	                        username: username,
                            password: password,
                            token: token
                    	}), function() {
                            angular.extend(svc.user, {
                                username: username,
                                authenticated: true,
                                token: token
                            });

                            deferred.resolve();
                        });
                }
                else if (response.data == 'Invalid device date/time')
                {
                    deferred.reject({status: 1, message: response.data});
                }
                else {
                    deferred.reject();
                }
            }, function(error) {
	            svc.loggingIn = false;
                if (cancelled) {
                    deferred.reject({ cancelled: true });
                } else {
                    deferred.reject(error);
                }
            });

            return { 
                promise: deferred.promise,
                cancel: cancel
            };
        }
    };

    return svc;
}]);

vmModule.factory('vmVersioning', ['$http', 
                                  '$q',
                                  '$timeout',
                                  '$log',
                                  'persistantStorage',
                                  'ngToast',
                                  '$rootScope',
                                  '$sce',
                                  'vmSettings',
                                  function ($http, $q, $timeout, $log, persistantStorage, ngToast, $rootScope, $sce, vmSettings) {
    var toastScope = $rootScope.$new(true);
    var toast = null;

	var vmVersioningService = {
		getServerVersionHash: function(fullurl) {
            var deferred = $q.defer();

            // we add the handle errors header manually here as we may not have downloaded
            // bworkflow-error-handling so can't rely on it and implicitly call specificallyHandled
            // however it may be loaded, so we need to be able to tell it not to worry about broadcasting
            // the error if it occurs.
            $http({
                method: 'GET',
                url: fullurl + '/api/v1/app/VersionHash?nonce=' + Math.random(),
                headers: { 'Bworkflow-Handle-Errors-Generically': false }
            }).then(function (response) {
                deferred.resolve(response.data);
            }, function (reason) {
                $log.log('An error occurred getting the version hash of the server');
                deferred.reject(reason);
            });
            
            return deferred.promise;
        },
        getLocalVersionHash: function(cb) {
            persistantStorage.getItem('resourceversionhash', function(version) {
                cb(version);
            });
        },
        setLocalVersionHash: function(hash, success) {
            $log.info('Setting local version hash to ' + hash);
        	persistantStorage.setItem('resourceversionhash', hash, success);    
        },
		breakOutVersionHash: function(hash) {
            // the different parts of the hash are seperated by \r\n's
            return hash.match(/[^\r\n]+/g);
        },
        breakOutUrl: function(tag, attribute) {
            // so each part of the hash is either a script or a link tag, so we
            // can get at the URL by treating it as XML and extracting the attribute
            // we want, this function helps with that.
            var dom = jQuery.parseXML(tag);
            
            return $(dom.documentElement).attr(attribute);
        },
        requestFileSystem: function() {
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024*1024*20, function(fs) {
            	vmVersioningService.fs = fs;
            }, function(error) {
				vmVersioningService.fs = null;
                $log.log('File system request failed');
            });  
        },
        writeFile: function(filename, data, type) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null) {
                // no file system support, in this scenario we don't do anything
                // with the file
                $timeout(function() {
                   deferred.resolve(); 
                });
                
                return deferred.promise;
            }
            
        	var path = cordova.file.dataDirectory;
            
            $log.debug('data directory is ' + path);
            
            window.resolveLocalFileSystemURL(path, function(dirEntry) {
                $log.debug('data directory resolved');
                $log.debug(JSON.stringify(dirEntry));
                
                dirEntry.getFile(filename, {create: true, exclusive: false}, function(fileEntry) {
                   $log.debug('getFile for ' + filename + ' succeeded');
                   $log.debug(JSON.stringify(fileEntry));
                    
                   fileEntry.createWriter(function(writer) {
                       $log.debug('file writer created');
                       writer.onwriteend = function(e) {
                         deferred.resolve();  
                       };
                       writer.onerror = function(e) {
                           deferred.reject(e);
                       };
                       
                       var blob = new Blob([data], { type: type || 'text/plain' });
                       writer.write(blob);
                   }); 
                });
            });
            
            return deferred.promise;
        },
        deleteFile: function(filename) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null) {
                // no file system support, in this scenario we don't do anything
                // with the file
                $timeout(function() {
                   deferred.resolve(); 
                });
                
                return deferred.promise;
            }
            
            var path = cordova.file.dataDirectory;
            
            $log.debug('data directory is ' + path);
            
            window.resolveLocalFileSystemURL(path, function(dirEntry) {
                $log.debug('data directory resolved');
                $log.debug(JSON.stringify(dirEntry));

                dirEntry.getFile(filename, {}, function(fileEntry) {                
                    fileEntry.remove(deferred.resolve, deferred.reject);
                });
            });
            
            return deferred.promise;
        },
        resetToAppStoreRelease: function () {
            // This will delete all files/directories in the dataDirectory (where App updates are stored)
            // Note, we *do not delete* 'localStorage.json' since this stores our VM WebServer URL otherwise User has to re-enter this
            var alldeferred = $q.defer();
            var ignoreFiles = ['localStorage.json'];
            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {
                var dirReader = dirEntry.createReader();
                dirReader.readEntries(function (entries) {
                    var qall = [];
                    entries.forEach(function (entry) {
                        if (entry.isFile) {
                            if (ignoreFiles.indexOf(entry.name) >= 0) {
                                return;
                            }
                            var deferred = $q.defer();
                            entry.remove(function () {
                                deferred.resolve();
                            }, function () {
                                deferred.reject();
                            });
                            qall.push(deferred.promise);
                        } else if (entry.isDirectory) {
                            var deferred = $q.defer();
                            entry.removeRecursively(function () {
                                deferred.resolve();
                            }, function () {
                                deferred.reject();
                            });
                            qall.push(deferred.promise);
                        }
                    });
                    $q.all(qall).then(function () {
                        alldeferred.resolve();
                    }, function () {
                        alldeferred.reject();
                    });
                });
            });
            return alldeferred.promise;
        },
        updateResourcesIfRequired: function () {
            // Prevent reentry
            if (vmVersioningService.$updateResourcesIfRequired) {
                return;
            }
            vmVersioningService.$updateResourcesIfRequired = true;
            var allowEntryAgain = function (result) {
                delete vmVersioningService.$updateResourcesIfRequired;
                return result;
            }

            if (toast) {
                ngToast.dismiss(toast);
                toast = null;
            }

            var deferred = $q.defer();

            // Make sure we allow entry once the promise resolves/rejects
            deferred.promise.then(allowEntryAgain, allowEntryAgain);

            var deferredSuccess = function() {
                deferred.resolve(httpDaemon.url);
            }
            var deferredFail = function(err) {
                deferred.reject(err);
            }

            vmSettings.getSettings(function (settings) {
                var fullurl = settings.fullUrl;
                // first get the server hash
                vmVersioningService.getServerVersionHash(fullurl).then(function (response) {
                    var serverHash = response + settings.debugMode;         // DebugMode affects the hash so we redownload automatically when its changed
                    vmVersioningService.getLocalVersionHash(function (localHash) {
                        $log.info('ServerHash = ' + serverHash);
                        $log.info('LocalHash = ' + localHash);
                        if (serverHash == localHash) {
                            // all good
                            $log.info('Local hash and server hash match');
                            deferred.resolve('');
                            return;
                        }

                        toast = ngToast.info({
                            content: $sce.trustAsHtml("<div>{{ updateMessage }}</div><div>{{ instructions }}</div>"),
                            compileContent: toastScope,
                            dismissOnClick: false,
                            dismissOnTimeout: false
                        });

                        var toastMessage = function (msg, instructions, dismissTime) {
                            $timeout(function () {
                                toastScope.updateMessage = msg;
                                toastScope.instructions = instructions;
                                $log.info(msg);

                                if (angular.isDefined(dismissTime)) {
                                    $timeout(function () {
                                        ngToast.dismiss(toast);
                                    }, dismissTime);
                                }
                            });
                        }

                        // ok, so they're different, we need to download what's on the server
                        $log.info('Local hash and server hash are not equal, starting download and caching of server resources');

                        var skipCache = '';
                        skipCache = 'nonce=' + Math.random();
                        toastMessage('Downloading update..');

                        var fileTransfer = new FileTransfer();
                        var uri = encodeURI(fullurl + '/api/v1/assets/appbundle?appendSHA1=true&' + skipCache + '&seperateFiles=' + settings.debugMode);

                        fileTransfer.onprogress = function (progressEvent) {
                            if (progressEvent.lengthComputable) {
                                var percent = ((progressEvent.loaded * 100) / progressEvent.total).round();
                                toastMessage('Downloading update (' + percent + '%)');
                            } else {
                                toastMessage('Downloading update (' + progressEvent.loaded.bytes(2) + ')');
                            }
                        };

                        fileTransfer.download(uri, cordova.file.dataDirectory + 'bundle.zip', function (entry) {

                            toastMessage('Checking update..');

                            entry.file(function (file) {
                                var reader = new FileReader();

                                reader.onload = function (evt) {
                                    var fileBuffer = evt.target.result;
                                    // Last 20 bytes is the SHA1 of the file
                                    var fileBytes = new Uint8Array(fileBuffer, 0, fileBuffer.byteLength - 20);
                                    var sha1Bytes = new Uint8Array(fileBuffer, fileBuffer.byteLength - 20, 20);
                                    var wordArray = CryptoJS.lib.WordArray.create(fileBytes);
                                    var calculatedSHA1words = CryptoJS.SHA1(wordArray);
                                    var calculatedSHA1 = CryptoJS.enc.Hex.stringify(calculatedSHA1words);

                                    var expectedSHA1words = CryptoJS.lib.WordArray.create(sha1Bytes);
                                    var expectedSHA1 = CryptoJS.enc.Hex.stringify(expectedSHA1words);

                                    $log.info('Calculated SHA1 ' + calculatedSHA1 + ', Exptected SHA1 = ' + expectedSHA1);
                                    if (calculatedSHA1 == expectedSHA1) {
                                        toastMessage('Applying update..');
                                        zip.unzip(cordova.file.dataDirectory + 'bundle.zip', cordova.file.dataDirectory, function (zipResult) {
                                            if (zipResult == 0) {
                                                // We are done, have received and unzipped full bundle
                                                vmVersioningService.setLocalVersionHash(serverHash, function () {
                                                    toastMessage('Update finished');
                                                    vmVersioningService.deleteFile('bundle.zip').then(deferredSuccess, deferredSuccess);
                                                });
                                            } else {
                                                toastMessage('Update fail (Zip ' + zipResult + ')');

                                                // Failed to Unzip, reset ourselves back to AppStore release
                                                vmVersioningService.resetToAppStoreRelease().then(function() {
                                                    toastMessage('Update failed', 'Please contact Virtual Manager support');
                                                    deferredFail();
                                                }, function() {
                                                    toastMessage('Update failed, AppStore reset failed', 'Please contact Virtual Manager support');
                                                    deferredFail();
                                                });
                                            }
                                        });
                                    } else {
                                        $log.info('Mismatched SHA1');
                                        toastMessage('Update fail (Corrupt)', '', 5000);
                                        deferredFail();
                                    }
                                };
                                reader.onerror = function (err) {
                                    toastMessage('Update fail (File read ' + err + ')', '', 5000);
                                    deferredFail();
                                }
                                reader.readAsArrayBuffer(file);
                            });
                        }, function () {
                            toastMessage('Downloading JS Resources');
                            // There is no ZIP Bundle, revert to seperate JS, CSS and HTML Template bundles ..
                            $http.get(fullurl + '/api/v1/assets/jsbundle?id=jsbundle_ios&' + skipCache).then(function (jsResponse) {
                                $log.info('JS bundle retrieved from server');
                                vmVersioningService.writeFile('bundle.js', jsResponse.data).then(function () {
                                    $log.info('JS bundle saved to local file system, starting retrieval of CSS bundle');
                                    toastMessage('Downloading CSS Resources');

                                    $http.get(fullurl + '/api/v1/assets/cssbundle?id=cssbundle_ios&' + skipCache).then(function (cssResponse) {
                                        $log.info('CSS bundle retrieved from server');
                                        vmVersioningService.writeFile('bundle.css', cssResponse.data).then(function () {
                                            $log.info('CSS bundle saved to local file system, starting retrieval of HTML templates');
                                            $http.get(fullurl + '/api/v1/assets/templatebundle?' + skipCache).then(function (templateResponse) {
                                                $log.log('HTML template bundle retrieved from server');
                                                vmVersioningService.writeFile('bundle.html', templateResponse.data).then(
                                                function () {
                                                    $log.info('HTML template bundle saved to local file system, all resources have been downloaded and cached');
                                                    vmVersioningService.setLocalVersionHash(serverHash, function () {
                                                        toastMessage('Update Finished');
                                                        deferred.resolve(baseUrl);
                                                    });
                                                }, function (templateError) {
                                                    toastMessage('Template write error', '', 3000);
                                                    $log.error('An error occurred writing the template bundle to the device file api');
                                                    deferred.reject(templateError);
                                                });
                                            }, function (templateError) {
                                                toastMessage('Template read error', '', 3000);
                                                $log.error('An error occurred getting the template bundle from the server');
                                                deferred.reject(templateError);
                                            });
                                        }, function (cssError) {
                                            toastMessage('CSS write error', '', 3000);
                                            $log.error('An error occurred writing the css bundle to the device file api');
                                            deferred.reject(cssError);
                                        });
                                    }, function (reason) {
                                        toastMessage('CSS read error', '', 3000);
                                        $log.error('An error occurred getting the CSS bundle from the server');
                                        deferred.reject(reason);
                                    });
                                }, function (error) {
                                    toastMessage('JS write error', '', 3000);
                                    $log.error('An error occurred writing the js bundle to the device file api');
                                    deferred.reject(error);
                                });
                            }, function (reason) {
                                toastMessage('JS read error', '', 3000);
                                $log.log('An error occurred getting the js bundle from the server');
                                deferred.reject(reason);
                            });
                        });
                    });
                }, function (reason) {
                    $log.log('An error occurred getting the version hash from the server');
                    deferred.resolve('');
                });
            });

            return deferred.promise;
        },
        jsBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/jsbundle?id=jsbundle_ios';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.js';
            }
        },
        cssBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/cssbundle?id=cssbundle_ios';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.css';
            }
        },
        templateBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/templatebundle';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.html';
            }
        },
        templateBundle: function(fullurl) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
				// we download the bundle from the server in this instance
            	$http.get(fullurl + '/api/v1/assets/templatebundle').then(function(templateResponse) {
                	deferred.resolve(templateResponse.data);    
                }, function(templateError) {
                    $log.log('An error was encountered downloading the template bundle from the server');
                    deferred.reject(templateError);
                });
            }
            else {
				// we have it local, so get it from there
                
            }
            
            return deferred.promise;
        }
    };
                          
    vmVersioningService.requestFileSystem();
                                      
    return vmVersioningService;
}]);

vmModule.factory('spinnerDialog', [function() {
    return (window.plugins ? window.plugins.spinnerDialog : null) || {
        show: function() {            
        },
        hide: function() {            
        }        
    }
}]);

vmModule.factory('appDevice', ['$interval', '$timeout', function($interval, $timeout) {
    var svc = angular.extend({}, window.device);

    svc.uuid = svc.uuid.toUpperCase();

    var wifiWizard = window.WifiWizard;
    if (angular.isDefined(wifiWizard)) {
        function checkWifi() {
            wifiWizard.getCurrentSSID(function (ssid) {
                $timeout(function () {
                    svc.ssid = ssid;
                });
            }, function (error) {
                $timeout(function () {
                    svc.ssid = error;
                });
            });
        }

        $interval(checkWifi, 60 * 1000);
        checkWifi();
    }

    return svc;
}]);

vmModule.factory('appVersion', [function() {
    var svc = {
        appName: 'VM Player', 
        versionNumber: 'Version',
        versionCode: 'Code',
        packageName: 'Package'
    };
    
    if (cordova && cordova.getAppVersion) {
        cordova.getAppVersion.getAppName(function(value) { 
			svc.appName = value;                                                   
		});
        cordova.getAppVersion.getVersionNumber(function(value) { 
			svc.versionNumber = value;                                                   
		});        
        cordova.getAppVersion.getVersionCode(function(value) { 
			svc.versionCode = value;                                                   
		});        
        cordova.getAppVersion.getPackageName(function(value) { 
			svc.packageName = value;                                                   
		});        
    }
    
    return svc;
}])

vmModule.factory('onSnapRemote', ['snapRemote', '$timeout', function (snapRemote, $timeout) {
    var events = Object.create(null);

    return function (event, callback) {
        snapRemote.getSnapper().then(function (snapper) {
            var list = events[event];
            if (angular.isUndefined(list)) {
                list = [];
                events[event] = list;
                snapper.on(event, function () {
                    $timeout(function () {
                        angular.forEach(list, function (cb) {
                            cb();
                        })
                    });
                })
            }
            list.push(callback);
        });
    }
}])

// This controller handles the movement through the various states required for the app
// to run. These follow the following workflow.
//    1. URL - A Url must be supplied if one isn't stored in settings. If a URL is present in settings, we go to step 2
//    2. Authentication - The user must authenticate against the URL defined in 1.
//    3. Test resource versions - We test the server to make sure the JS and CSS we have for the main part of the app are up to date
//        3A. If resources aren't up to date or we don't have them, then download them and store in the local file system
//    4. Steps 1,2,3 are all good, ok navigate off to eh app.html page, which will handle things from here
vmModule.controller('loaderController', ['$scope',
                                         '$rootScope', 
                                         'vmSettings',
                                         'vmAuthenticate',
                                         'vmVersioning',
                                         'vmLocation',
                                         '$q',
                                         '$log', 
                                         '$window', 
                                         'spinnerDialog',
                                         'appVersion',
                                         '$timeout',
                                         '$http',
                                         '$filter',
                                         'appDevice',
                                         'persistantStorage',
                                         'onSnapRemote',
                                         loaderController]);
function loaderController($scope, $rootScope, vmSettings, vmAuthenticate, vmVersioning, vmLocation, $q, $log, $window, spinnerDialog, appVersion, $timeout, $http, $filter, appDevice, persistantStorage, onSnapRemote) {
    spinnerDialog.hide();

    $scope.appVersion = appVersion;    
    $scope.appDevice = appDevice;

    $scope.auth = vmAuthenticate;
    vmAuthenticate.lastLogin(function(data) {
        $scope.login = data || {};

        // Try cached last login ..
        if ($scope.login && $scope.login.username) {
    //        alert('STOP 1 - Attach Debugger');
            $timeout(function() {
    //            debugger;
                $scope.loginNow($scope.login.username, $scope.login.password, true);
            }, 1000);
        }
    });

    vmSettings.getSettings(function(settings) {
        $scope.settings = settings;
        if (settings.url != null) {
            // Ok, we have a tenant, check for bundle update ..
            vmVersioning.updateResourcesIfRequired().then(appUpdated => {
                if (appUpdated) {
                    window.location.reload();
                }
            });
        }
    });

    vmSettings.onProfileChange(function (tabletProfile) {
        $scope.tabletProfile = tabletProfile;
        $scope.allowNumberPadLogin = tabletProfile && tabletProfile.allownumberpadlogin;
        $scope.showNumberpad = $scope.allowNumberPadLogin;
    });

    $scope.toggleNumberpad = function() {
        $scope.showNumberpad = !$scope.showNumberpad;
    }
    $scope.state = 'url';
    
    $log.debug('load controller loaded with ' + JSON.stringify($scope.settings));
    
    $scope.showConsole = false;
    
    $scope.resourceMessages = [];
    
    // just pre-load the application config (makes debugging issues a little easier)
    vmSettings.getConfig();
    
    $scope.calculateState = function() {
        vmSettings.hasSetting('url', function (has) {
            $scope.state = has ? 'login' : 'url';
        });
    };
 
    $scope.$on('saved', function() {
        $scope.calculateState();            
    });

    // Check support for CameraPreview for EVS-1311 Login photo
    $scope.loginPhotoSupported = angular.isDefined(window.CameraPreview);

    if ($scope.loginPhotoSupported) {
        $scope.loginPhotoState = 'hidden';

        function initCamera() {
            vmSettings.getProfile(function (tabletProfile) {
                if (tabletProfile.loginphotoevery) {
                    persistantStorage.getItem('loginCount', function (loginCount) {
                        loginCount = loginCount || 0;

                        if (loginCount % tabletProfile.loginphotoevery == 0) {
                            $scope.loginUserIdChanged = function (userId) {
                                if ($scope.loginPhotoState == 'hidden') {
                                    $scope.loginPhotoState = 'starting';

                                    CameraPreview.startCamera({
                                        x: window.screen.width / 4,
                                        y: window.screen.height * 3 / 8,
                                        width: window.screen.width / 2,
                                        height: window.screen.height / 4,
                                        camera: CameraPreview.CAMERA_DIRECTION.FRONT,
                                        toBack: false,
                                        tapPhoto: false,
                                        tapFocus: false,
                                        previewDrag: false
                                    }, function () {
                                        $scope.loginPhotoState = 'preview';
                                    }, function (error) {
                                        $scope.loginPhotoState = 'error';
                                    });
                                }
                            }
                        }
                    });
                }
            });
        }

        // Stop the camera (ignore if already stopped) then hook Username change ready to show
        CameraPreview.stopCamera(initCamera, initCamera);
    }

    function takePhoto() {
        if ($scope.loginPhotoState == 'preview') {
            var deferred = $q.defer();
            CameraPreview.takePicture({
                width: 800,
                height: 600,
                quality: 95
            }, function (photoData) {

                // Resolve with a function that will Stop the Camera and Upload the photo, should only be called if Login is successful
                deferred.resolve(function () {
                    CameraPreview.stopCamera(function () {
                        $scope.loginPhotoState = 'hidden';
                    });
                    $scope.loginPhotoState = 'stopping';
                    return $http({
                        url: $scope.settings.fullUrl + '/API/v1/Media/UploadLoginPhoto',
                        method: 'POST',
                        data: photoData[0],
                        headers: {
                            'Content-Type': 'image/jpg'
                        }
                    });
                });
            }, function (error) {
                deferred.reject(error);
            })

            return deferred.promise;
        } else {
            return $q.when(function () {
                return $q.when();
            })
        }
    }

    function loadDashboard(result) {
        vmLocation.href(httpDaemon.url + 'dashboard.html');
    }

    function updateResourcesIfRequired() {
        $scope.state = 'checkresources';
        return vmVersioning.updateResourcesIfRequired().then(
            function (result) {
                $scope.state = 'loggedin';
            });
    }

    function updateTabletProfile() {
        return vmSettings.updateProfile($scope.settings.fullUrl);
    }

    function loginWithUserPass(username, password) {
        $log.info('Attempting to login to ' + $scope.settings.fullUrl);

        $scope.login.failed = false;
        delete $scope.login.error;

        var loginRequest = vmAuthenticate.login($scope.settings.fullUrl, username, password);
        spinnerDialog.show('Logging in', null, loginRequest.cancel);

        return loginRequest.promise;
    }

    function ignoreAfterLoginError(error) {
        $log.error(error);
    }

    function onLoginError(error) {
        // failure
        $scope.state = 'login';
        spinnerDialog.hide();

        $log.error('Login to ' + $scope.settings.fullUrl + ' failed ' + JSON.stringify(error));
        if (!error || !error.cancelled) {
            $scope.login.failed = true;
            $scope.login.error = error;
            $scope.login.usernumber = '';
        }

        return $q.reject(error);
    }

    function incrementLoginCount() {
        var deferred = $q.defer();
        persistantStorage.getItem('loginCount', function (loginCount) {
            loginCount = (loginCount || 0) + 1;

            persistantStorage.setItem('loginCount', loginCount, function () {
                deferred.resolve();
            }, function (error) {
                deferred.reject(error);
            });
        }, function (error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    $scope.loginNow = function (username, password, autoLogin) {
        takePhoto().then(function (uploadPhoto) {
            return loginWithUserPass(username, password).then(function () {
                // Increase login count so we know when to show LoginCamera next ..

                // Now we have logged in succesfully, we will perform some ancillory functions but MUST ALWAYS load the Dashboard as login was success
                return incrementLoginCount().catch(ignoreAfterLoginError)
                    .then(uploadPhoto).catch(ignoreAfterLoginError)
                    .then(updateTabletProfile).catch(ignoreAfterLoginError)
                    .then(updateResourcesIfRequired).catch(ignoreAfterLoginError)
                    .then(loadDashboard);
            }).catch(onLoginError);
        })
    };
    
    $scope.refresh = function () {
        vmLocation.reload($scope.settings.fullUrl);   
    }
    
    $scope.logout = function () {
		// Show the login page  
        persistantStorage.removeItem('user', function() {            
            $scope.state = 'login';
            vmLocation.href('index.html');
        });
    };
    
    $scope.gotoSettings = function () {
        if (angular.isDefined($scope.tabletProfile) && $scope.tabletProfile != null && angular.isDefined($scope.tabletProfile.settingspassword) == true && $scope.tabletProfile.settingspassword != null && $scope.tabletProfile.settingspassword != '') {
            if (prompt('Enter Settings Password') != $scope.tabletProfile.settingspassword) {
                return;
            }
        }


        $scope.state = 'url';
    };
    
    $scope.toggleConsole = function() {
      $scope.showConsole = !$scope.showConsole;  
    };
    
    $scope.showUrl = function() { 
    	$scope._showUrl = true;
    }
    
    $scope._showUrl = false;
    $scope.calculateState();

    $scope.inKioskMode = false;

    if (window.KioskPlugin) {
        onSnapRemote('open', function() {
            window.KioskPlugin.isSetAsLauncher(function(inKiosk) {
                $timeout(function() {
                    $scope.inKioskMode = inKiosk;
                });
            });
        });
    }

    $scope.exitKioskMode = function() {
        if ($scope.tabletProfile && $scope.tabletProfile.exitKioskModePassword) {
            if (prompt('Enter tablet profile password to exit Kiosk mode') == $scope.tabletProfile.exitKioskModePassword) {
                window.KioskPlugin.exitKiosk();
            }
        } else {
            if (prompt('Enter password to exit Kiosk mode') == 'k!osk123') {
                window.KioskPlugin.exitKiosk();
            }
        }
    }
}

/*
vmModule.controller('loginPhotoTestCtrl', [
    '$scope',
    '$q',
    '$log',
    '$window',
    '$http',
    function($scope, $q, $log, $window, $http) {
        CameraPreview.startCamera({
            x: window.screen.width / 4,
            y: window.screen.height * 3 / 8,
            width: window.screen.width / 2,
            height: window.screen.height / 4,
            camera: CameraPreview.CAMERA_DIRECTION.FRONT,
            toBack: false,
            tapPhoto: false,
            tapFocus: false,
            previewDrag: false
        });

        $scope.takePhoto = function () {
            CameraPreview.takePicture({
                width: 800,
                height: 600,
                quality: 95
            }, function (photoData) {

            $http({
                url: 'http://192.168.20.14/portal/API/v1/Media/UploadLoginPhoto',
                method: 'POST',
                data: photoData[0],
                headers: {
                    'Content-Type': 'image/jpeg'
                }
            });
        })
    }
}])
*/

// This controller handles the movement through the various states required for the app
// to run. These follow the following workflow.
//    1. URL - A Url must be supplied if one isn't stored in settings. If a URL is present in settings, we go to step 2
//    2. Authentication - The user must authenticate against the URL defined in 1.
//    3. Test resource versions - We test the server to make sure the JS and CSS we have for the main part of the app are up to date
//        3A. If resources aren't up to date or we don't have them, then download them and store in the local file system
//    4. Steps 1,2,3 are all good, ok navigate off to eh app.html page, which will handle things from here
vmModule.controller('dashboardController', [
    '$scope',
    '$rootScope', 
    'vmSettings',
    'vmAuthenticate',
    'vmVersioning',
    'vmLocation',
    '$q',
    '$log', 
    '$window', 
    'spinnerDialog',
    'appVersion',
    'appDevice',
    '$http',
    'languageTranslate',
    dashboardController
]);
function dashboardController($scope, $rootScope, vmSettings, vmAuthenticate, vmVersioning, vmLocation, $q, $log, $window, spinnerDialog, appVersion, appDevice, $http, languageTranslate) {
    spinnerDialog.hide();

    $scope.appVersion = appVersion;    
    $scope.appDevice = appDevice;

    $scope.auth = vmAuthenticate;

    $scope.location = vmLocation;

    document.addEventListener('resume', function () {
        // When we resume, if we have no Cordova plugins then try to reload and relogin ..
        if (!cordova || !cordova.plugins || !cordova.plugins.CorHttpd) {
            vmLocation.href('index.html');
        }
    }, false);

    vmSettings.getSettings(function(settings) {
        $scope.settings = settings;

        $http.get(settings.fullUrl + '/api/v1/app/webappversion?nonce=' + Math.random()).then(function (data) {
            appVersion.webAppVersion = data.data;
        });

        if (window.iOS13Fix_DashboardLoaded === 1) {
            delete window.iOS13Fix_DashboardLoaded;
            // Ok, dashboard has just loaded after we logged in, if at this point we load "lastLogin" data and there is a token then
            // we are good, if there is no token then this is the 1st time we have logged in after the iOS13Fix patch has been applied
            // in which case the login would have tried to store cookies (which fail in iOS13) so we now have to perform a 2nd background login
            vmAuthenticate.getUser().then(function (user) {
                if (!user.token) {
                    vmAuthenticate.login(settings.fullUrl, user.username, user.password).promise.then(function () {
                        vmLocation.href(httpDaemon.url + 'dashboard.html');
                    })
                }
            })
        }
    });
    
    var _tabletProfile = {};
    vmSettings.onProfileChange(function(tabletProfile) {
        _tabletProfile = tabletProfile || {};
    });

    // just pre-load the application config (makes debugging issues a little easier)
    vmSettings.getConfig();
    
    $scope.refresh = function () {
        vmLocation.reload($scope.settings.fullUrl); 
    }
    
    $scope.logout = function (ignoreDialog) {
        if (angular.isDefined(_tabletProfile) && _tabletProfile != null && angular.isDefined(_tabletProfile.logoutpassword) == true && _tabletProfile.logoutpassword != null && _tabletProfile.logoutpassword != '') {
            if (prompt('Enter Logout Password') != _tabletProfile.logoutpassword) {
                return;
            }
        }

        if (!ignoreDialog) {
            switch(_tabletProfile.confirmlogout) {
                case "SimpleConfirm":
                    if (!confirm("Are you sure you want to logout?")) {
                        return;
                    }
                    break;
                
                default:
                    break;
            }
        }

		// Show the login page  
        vmAuthenticate.logout(function () {        
            vmLocation.href('index.html');
        });
    };

    $rootScope.$on('auto-logout.execute', function(event, args) {
        $scope.logout(true);
    })
    
    $scope.choosingLanguage = false;
    $scope.toggleChoosingLanguage = function () {
        $scope.choosingLanguage = !$scope.choosingLanguage;
    }

    $scope.getCurrentLanguage = function () {
        return languageTranslate.currentLanguage;
    };

    $scope.browserSelectLanguage = function (culturename) {
        $scope.choosingLanguage = false;
        languageTranslate.browserSelectLanguage(culturename);
    };

    $scope.languages = {};

    languageTranslate.getLanguages().then(function (data) {
        $scope.languages = data;
    });

}

vmModule.controller('settingsController', ['$scope', 
                                         'vmSettings',
                                         'vmAuthenticate',
                                         'vmVersioning',
                                         'vmLocation',
                                         '$q',
                                         '$log',
                                         '$window',
                                         settingsController]);
function settingsController($scope, vmSettings, vmAuthenticate, vmVersioning, vmLocation, $q, $log, $window) {
	vmSettings.getSettings(function(settings) {
        $scope.settings = settings; 
    });
 	$scope.showConsole = false;
    
    $scope.saveUrl = function() {
        vmSettings.getConfig().then(function(config) {
            $scope.settings.fullUrl = $scope.settings.url + '/' + config.virtualapp;
            vmSettings.setSettings($scope.settings, function () {
                vmLocation.href('index.html');  
            });
        });
    };
    
    $scope.toggleConsole = function() {
      $scope.showConsole = !$scope.showConsole;  
    };
}

vmModule.directive('logViewer', ['$log',
   function($log) {
       return {
           templateUrl: 'templates/log-viewer.html',
           restrict: 'E',
           replace: true,
           transclude: true,
           link: function(scope, elt, attrs) {
				scope.log = $log;
           }
       };
   } 
]);

vmModule.directive('settingsUrl', ['$timeout', '$log', 'vmSettings', 'vmVersioning',
   function($timeout, $log, vmSettings, vmVersioning) {
       return {
           templateUrl: 'templates/settings-url.html',
           restrict: 'E',
           replace: true,
           transclude: true,
           require: 'ngModel',
           scope: {
               settings: '=ngModel'
           },
           controller: function($scope) {
           		$scope.state = 'unknown';

                $scope.checkUrl = function () {
                    $scope.state = 'checking';

                    vmSettings.checkUrl($scope.settings.url).then(function () {
                        $scope.state = 'valid';
                    }, function () {
                        $scope.state = 'invalid';
                    });
                }

                var debounceCheckUrl = $scope.checkUrl.debounce(2000);

                $scope.$watch('settings.url', function (newvalue, oldvalue) {
                    if (newvalue) {
                        $scope.state = 'changing';
                        debounceCheckUrl();
                    } else {
                        debounceCheckUrl.cancel();
                        $scope.state = 'invalid';
                    }
                });
               
               $scope.clearUrl = function() {
                   $scope.settings.url = '';
               }
               
               $scope.scan = function() {
                   cordova.plugins.barcodeScanner.scan(
                       function(result) {
                           $timeout(function() {
                               $scope.settings.url = result.text;
                               if ($scope.settings.url.startsWith('http://')) {
                                   $scope.settings.url = $scope.url.substr(7);
                               } else if ($scope.settings.url.startsWith('https://')) {
                                   $scope.settings.url = $scope.settings.url.substr(8);
                               }
                           });
                       },
                       function(error) {
                           
                       },
                   	   {
                        	"preferFrontCamera" : false, // default false
                        	"showFlipCameraButton" : true // default false
                   	   }
                   );
               }
               
               $scope.saveUrl = function(success) {
                   $scope.settings.fullUrl = null;
                   vmSettings.setSettings($scope.settings, function() {
                       // Ok, we now download the AppBundle from the Tenant :)
                       vmVersioning.updateResourcesIfRequired().then(appUpdated => {
                           if (appUpdated) {
                               window.location.reload();
                           } else {
                               $scope.$emit('saved');
                               if (success) {
                                   success();
                               }
                           }
                       });
                   });
               }
           }
       };
   } 
]);

vmModule.controller('dashboardListener', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.dashboards = [];
    
    $rootScope.$on('dashboards', function(event, args) {
        $scope.dashboards = args.dashboards;

        $scope.selectDashboard = function (db) {
            $scope.$emit('tab_selected');           // $rootScope will receive and fire invalidatemap & fill-height.changed events to refresh these suckers
        }
    })
}]);

/**
  * JavaScript implementation of Trilateration to find the position of a
  * point (P4) from three known points in 3D space (P1, P2, P3) and their
  * distance from the point in question.
  *
  * The solution used here is based on the derivation found on the Wikipedia
  * page of Trilateration: https://en.wikipedia.org/wiki/Trilateration
  *
  * This library does not need any 3rd party tools as all the non-basic
  * geometric functions needed are declared inside the trilaterate() function.
  *
  * See the GitHub page: https://github.com/gheja/trilateration.js
  */

/**
  * Calculates the coordinates of a point in 3D space from three known points
  * and the distances between those points and the point in question.
  *
  * If no solution found then null will be returned.
  *
  * If two solutions found then both will be returned, unless the fourth
  * parameter (return_middle) is set to true when the middle of the two solution
  * will be returned.
  *
  * @param {Object} p1 Point and distance: { x, y, z, r }
  * @param {Object} p2 Point and distance: { x, y, z, r }
  * @param {Object} p3 Point and distance: { x, y, z, r }
  * @param {bool} return_middle If two solution found then return the center of them
  * @return {Object|Array|null} { x, y, z } or [ { x, y, z }, { x, y, z } ] or null
  */
vmModule.factory('vmMath', [function () {
    function sqr(a) {
        return a * a;
    }

    function norm(a) {
        return Math.sqrt(sqr(a.x) + sqr(a.y) + sqr(a.z));
    }

    function dot(a, b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    function vector_subtract(a, b) {
        return {
            x: a.x - b.x,
            y: a.y - b.y,
            z: a.z - b.z
        };
    }

    function vector_add(a, b) {
        return {
            x: a.x + b.x,
            y: a.y + b.y,
            z: a.z + b.z
        };
    }

    function vector_divide(a, b) {
        return {
            x: a.x / b,
            y: a.y / b,
            z: a.z / b
        };
    }

    function vector_multiply(a, b) {
        return {
            x: a.x * b,
            y: a.y * b,
            z: a.z * b
        };
    }

    function vector_cross(a, b) {
        return {
            x: a.y * b.z - a.z * b.y,
            y: a.z * b.x - a.x * b.z,
            z: a.x * b.y - a.y * b.x
        };
    }

    var deg2rad = function(deg) {
        return (deg * Math.PI) / 180.0;
    }

    var rad2deg = function(rad) {
        return (rad * 180.0) / Math.PI;
    }

    var distanceMetres = function(lat1, lon1, lat2, lon2) {
        var theta = lon1 - lon2;
        var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344 * 1000;          // Metres
        return dist;
    }

    function metres2Degrees(metres, latitude) {
        return metres / (1852 * 60 * Math.cos(deg2rad(latitude)));
    }

    function standardDeviation(values) {
        var avg = average(values);

        var squareDiffs = values.map(function (value) {
            var diff = value - avg;
            var sqrDiff = diff * diff;
            return sqrDiff;
        });

        var avgSquareDiff = average(squareDiffs);

        var stdDev = Math.sqrt(avgSquareDiff);
        return stdDev;
    }

    function average(data) {
        var sum = data.reduce(function (sum, value) {
            return sum + value;
        }, 0);

        var avg = sum / data.length;
        return avg;
    }

    return {
        distanceMetres: distanceMetres,
        deg2rad: deg2rad,
        rad2deg: rad2deg,
        metres2Degrees: metres2Degrees,
        standardDeviation: standardDeviation,
        average: average
    }
}]);

vmModule.factory('assetTrackerSvc', ['$rootScope', 'beaconSvc', '$geolocation', '$log', '$timeout', 'bworkflowApi', 'appDevice', 'persistantStorage', 'taskListService', '$interval', 'geolocation-registry', 'vmMath', 'vmSettings', 'rfc4122',
function ($rootScope, beaconSvc, $geolocation, $log, $timeout, bworkflowApi, appDevice, persistantStorage, taskListService, $interval, geolocationRegistry, vmMath, vmSettings, rfc4122) {

    const BeaconType = {
        Unknown: 0,
        Static: 1,
        Asset: 2
    }

    var _staticBeacons = Object.create(null);
    var _staticBeaconsFeed = null;
    $rootScope.$on('sectionUtils.addUrls', function () {
        _staticBeaconsFeed = bworkflowApi.createDataFeed({
            name: 'beacons',
            feed: 'Beacons',
            filter: "StaticLocationId ne null",
            orderbyfields: 'Id',
            idfields: ['Id'],
            usepaging: false,
            parameterdefinitions: []
        }, $rootScope);
        _staticBeaconsFeed.addAfterLoadHook(function (feed) {
            angular.forEach(feed.data, function (d) {
                _staticBeacons[d.alldata.Id] = d;

                d.latLng = L.latLng(d.alldata.Latitude, d.alldata.Longitude);
                d.altitude = angular.isDefined(d.alldata.Altitude) ? Number(d.alldata.Altitude) : 1.8 + Number(d.alldata.Level) * 3;
                d.projection = L.Projection.SphericalMercator.project(d.latLng);
            });
        });
        _staticBeaconsFeed.getData(true);
    });

    var _beacons = beaconSvc.getBeacons();

    var _serviceOptions;
    var executeServiceOptions = function () {
        if (!_serviceOptions) {
            return;
        }
        beaconSvc.configure(_serviceOptions);
    }

    var getServerServiceOptions = function () {
        bworkflowApi.execute('AssetTracker', 'GetServiceOptions')
            .then(function (data) {
                _serviceOptions = data;
                persistantStorage.setItem('beaconServiceOptions', JSON.stringify(data));

                executeServiceOptions();
            });
    };

    persistantStorage.getItem('beaconServiceOptions', function (data) {
        if (data) {
            _serviceOptions = JSON.parse(data);
            executeServiceOptions();
        }

        getServerServiceOptions();
    });

    var _fmaRecords = [];
    var _scanning = null;
    var $sendFMARecordsTimeout = null;

    function queueBeaconUpdate(beacon) {
        $log.info('Beacon ' + beacon.beaconId + ' changed to ' + beacon.range.name + ' distance ' + beacon.distance);
        _fmaRecords.push({
            br: {
                bid: beacon.beaconId,
                frutc: beacon.firstRangeUtc.toDate(),
                rng: beacon.range.id,
                rutc: beacon.timestamp.toDate(),
                bt: beacon.beaconType,
                bp: Math.round(beacon.battery)
            }
        })
    }

    function processBeaconUpdate(args) {
        if (angular.isDefined(args.expired)) {
            angular.forEach(args.expired, queueBeaconUpdate);
        }
        if (angular.isDefined(args.closerOrNew)) {
            angular.forEach(args.closerOrNew, queueBeaconUpdate);
        }
    }


    var startScanning = function () { 
        if (!_scanning) {
            _scanning = beaconSvc.startScanning(function (args) {
                processBeaconUpdate(args);

                // If we have beacon records to send then set a timeout to send them soon, this allows
                // us to gather multiple changes each which will reset the timeout before making the server call
                // Note, we do not let the change list grow more than 10 records to avoid continously resetting the
                // timeout
                if (_fmaRecords.length) {
                    if ($sendFMARecordsTimeout != null && _fmaRecords.length < 10) {
                        $timeout.cancel($sendFMARecordsTimeout);
                        $sendFMARecordsTimeout = null;
                    }

                    if ($sendFMARecordsTimeout == null) {
                        $sendFMARecordsTimeout = $timeout(function () {
                            var postingRecords = _fmaRecords.clone();
                            _fmaRecords.splice(0);

                            bworkflowApi.execute('AssetTracker', 'FMARecords', {
                                rid: rfc4122.v4(),
                                tim: moment.utc().toDate(),
                                tuuid: appDevice.uuid,
                                fma: postingRecords
                            });
                            // Clearing this allows the next post to occur
                            $sendFMARecordsTimeout = null;
                        }, 10000);           // Send in 10 seconds
                    }
                }
            });

        }
    }

    var stopScanning = function () {
        if (_scanning) {
            _scanning();
            _scanning = null;
        }
    }

    var startGPS = function () {
        $geolocation.watchPosition({
            enableHighAccuracy: true
        });
    };
    var stopGPS = function () {
        $geolocation.clearWatch();
    };

    // Service just runs in the background currently
    var svc = {
        queue: _fmaRecords,
        enableFMA: false,
        // These are not currently used (to keep it simple)
        bestStaticBeacon: null,
        closestAssetBeacons: []
    }

    // Enable FMA based on Tablet Profile
    var _tabletProfile = {};
    vmSettings.onProfileChange(function (tabletProfile) {
        _tabletProfile = tabletProfile || {};

        svc.enableFMA = _tabletProfile.enablefma;

        if (_tabletProfile.enablefma) {
            startScanning();

            $rootScope.$on('$geolocation.position.error', function (event, args) {
                startGPS();        // Try again
            });
            startGPS();
        } else {
            stopScanning();
        }
    });

    return svc;
}]);

vmModule.controller('beaconListener', ['$scope', 'assetTrackerSvc', 'beaconSvc', '$timeout', '$interval', 'onSnapRemote',
function($scope, assetTrackerSvc, beaconSvc, $timeout, $interval, onSnapRemote) {
    $scope.assetTrackerSvc = assetTrackerSvc;

    $scope.enabled = false;

    $scope.showBeacons = false;
    $scope.toggleShowBeacons = function () {
        $scope.showBeacons = !$scope.showBeacons;
    }

    // Beacons
    $scope.beacons = beaconSvc.getBeacons();  
    $scope.beaconSvc = beaconSvc;

    var beacon_led_flasher = $('#beacon.led_flasher');             // Cache this
    function flashBeaconLED() {
        // Sometimes Javascript loads quicker than the HTML and the above jQuery returns 0 elements ..
        if (!beacon_led_flasher.length) {
            beacon_led_flasher = $('#beacon.led_flasher');             // Cache this
        }
        beacon_led_flasher.removeClass('led_receive');
        $timeout(function () {
            beacon_led_flasher.addClass('led_receive');
        }, 0);
    }

    var assettracker_led_flasher = $('#assettracker.led_flasher');             // Cache this
    function flashSendLED() {
        // Sometimes Javascript loads quicker than the HTML and the above jQuery returns 0 elements ..
        if (!assettracker_led_flasher.length) {
            assettracker_led_flasher = $('#assettracker.led_flasher');             // Cache this
        }

        if (assetTrackerSvc.sendingLength) {
            assettracker_led_flasher.addClass('led_transmitting');

        } else if (assetTrackerSvc.sendingLength + assetTrackerSvc.queueLength == 0) {
            assettracker_led_flasher.removeClass('led_transmitting');
            assettracker_led_flasher.removeClass('led_receive');
            $timeout(function () {
                assettracker_led_flasher.addClass('led_receive');
            }, 0);
        }
    }

    var unwatchScanCount;
    var unwatchSendingLength;
    onSnapRemote('open', function () {
        $scope.enabled = assetTrackerSvc.enableFMA;

        unwatchScanCount = $scope.$watch('beaconSvc.scanCount', flashBeaconLED);
        unwatchSendingLength = $scope.$watch('assetTrackerSvc.sendingLength', flashSendLED);
    });
    onSnapRemote('close', function () {
        $scope.enabled = false;
        if (unwatchScanCount) {
            unwatchScanCount();
            unwatchScanCount = null;
        }
        if (unwatchSendingLength) {
            unwatchSendingLength();
            unwatchSendingLength = null;
        }
    });

}]);

vmModule.factory('environmentSensingSvc', ['$rootScope', 'environment-sensor-manager', '$log', '$timeout', 'bworkflowApi', 'appDevice', 'persistantStorage', '$interval', 'vmSettings', 'rfc4122',
    function ($rootScope, esMgr, $log, $timeout, bworkflowApi, appDevice, persistantStorage, $interval, vmSettings, rfc4122) {

        var _registeredProbes = Object.create(null);
        var _registeredProbesFeed = null;
        $rootScope.$on('sectionUtils.addUrls', function () {
            _registeredProbesFeed = bworkflowApi.createDataFeed({
                name: 'ESProbes',
                feed: 'ESProbes',
                orderbyfields: 'Id',
                idfields: ['Id'],
                usepaging: false,
                parameterdefinitions: []
            }, $rootScope);
            _registeredProbesFeed.addAfterLoadHook(function (feed) {
                angular.forEach(feed.data, function (d) {
                    _registeredProbes[d.alldata.Key] = d;      
                });
            });
            _registeredProbesFeed.getData(true);
        });

        var _probes = esMgr.getProbes();

        var _sensorRecords = [];
        var _scanning = null;
        var $sendRecordsTimeout = null;

        function queueSensorReading(sensor) {
            var serverProbe = _registeredProbes[sensor.probe.id];
            var record;
            if (serverProbe) {
                record = {
                    id: serverProbe.alldata.Id,
                    six: sensor.index,
                    val: sensor.value,
                    tim: sensor.readingTimestamp.toDate()
                };
            } else {
                // Unknown server probe
                record = {
                    key: sensor.probe.id,
                    model: sensor.probe.model,
                    serial: sensor.probe.serialNumber,
                    six: sensor.index,
                    st: sensor.type,
                    val: sensor.value,
                    tim: sensor.readingTimestamp.toDate()
                };
            }
            $log.info('ES record ' + JSON.stringify(record));
            _sensorRecords.push(record);
            sensor.sendTimestamp = moment.utc();
        }

        function sendAllSensors() {
            var before = _sensorRecords.length;
            angular.forEach(_probes, function (probe) {
                angular.forEach(probe.sensors, function (sensor) {
                    if (angular.isUndefined(sensor.sendTimestamp) || sensor.updateTimestamp.isAfter(sensor.sendTimestamp)) {
                        queueSensorReading(sensor);
                    }
                });
            });
            var after = _sensorRecords.length;
            $log.info('Queued ' + (after - before).toString() + ' ES records');
        }

        var startScanning = function () {
            if (!_scanning) {
                _scanning = esMgr.startScanning(function (sensors) {
                    angular.forEach(sensors, function (sensor) {
                        queueSensorReading(sensor);
                    });

                    if (_sensorRecords.length) {
                        if ($sendRecordsTimeout != null && _sensorRecords.length < 10) {
                            $timeout.cancel($sendRecordsTimeout);
                            $sendRecordsTimeout = null;
                        }

                        if ($sendRecordsTimeout == null) {
                            $sendRecordsTimeout = $timeout(function () {
                                var postingRecords = _sensorRecords.clone();
                                _sensorRecords.splice(0);

                                bworkflowApi.execute('EnvironmentSensing', 'SensorReadings', {
                                    rid: rfc4122.v4(),
                                    tim: moment.utc().toDate(),
                                    tuuid: appDevice.uuid,
                                    psr: postingRecords
                                }).then(function (response) {

                                });

                                $log.info('Sent ES ' + postingRecords.length.toString() + ' records');

                                // Clearing this allows the next post to occur
                                $sendRecordsTimeout = null;
                            }, 10000);           // Send in 10 seconds
                        }
                    }
                });

                // Send all sensor data every 10 minutes
                $interval(sendAllSensors, 10 * 60 * 1000);

                sendAllSensors();
            }
        };

        var stopScanning = function () {
            if (_scanning) {
                _scanning();
                _scanning = null;
            }
        }

        // Service just runs in the background currently
        var svc = {
            queue: _sensorRecords,
            enableES: false
        }

        // Enable ES based on Tablet Profile
        var _tabletProfile = {};
        vmSettings.onProfileChange(function (tabletProfile) {
            _tabletProfile = tabletProfile || {};

            svc.enableES = _tabletProfile.enableenvironmentalsensing;

            if (_tabletProfile.enableenvironmentalsensing) {
                startScanning();
            } else {
                stopScanning();
            }
        });

        return svc;
    }]);

vmModule.controller('environmentSensorListener', ['$scope', 'environmentSensingSvc', 'environment-sensor-manager', 'onSnapRemote', '$interval', '$timeout', function ($scope, esSvc, esMgr, onSnapRemote, $interval, $timeout) {
    $scope.probes = esMgr.getProbes();
    $scope.esSvc = esSvc;

    $scope.enabled = false;
    onSnapRemote('open', function () {
        $scope.enabled = esSvc.enableES;
    });
    onSnapRemote('close', function () {
        $scope.enabled = false;
    });

    var led_flasher = $('#esProbes.led_flasher');             // Cache this
    var _lastScanCount = 0;
    $scope.probeCount = 0;
    $interval(function () {
        if (esMgr.scanCount != _lastScanCount) {
            $scope.probeCount = $scope.probes.length;

            _lastScanCount = esMgr.scanCount;
            // Sometimes Javascript loads quicker than the HTML and the above jQuery returns 0 elements ..
            if (!led_flasher.length) {
                led_flasher = $('#esProbes.led_flasher');             // Cache this
            }
            led_flasher.removeClass('led_receive');
            $timeout(function () {
                led_flasher.addClass('led_receive');
            }, 0);
        }
    }, 500);

    $scope.toggleShowSensors = function () {
        $scope.showSensors = !$scope.showSensors;
    }
}]);

vmModule.directive('screenOrientationControl', ['$window', 'ngTimeout', 'persistantStorage', '$log', function ($window, ngTimeout, persistantStorage, $log) {
    var screenOrientation = angular.isDefined($window.screen) && angular.isDefined($window.screen.orientation) ? $window.screen.orientation : null;

    if (screenOrientation !== null && angular.isDefined(screenOrientation.isPlugin)) {
        return {
            template: "<div class='pull-right' ng-click='toggleLock()'><i ng-class=\"['icon-undo', 'icon-undo muted'][locked?1:0]\"></i>&nbsp<i ng-class=\"['icon-unlock muted','icon-lock'][locked?1:0]\"></i></div>",
            restrict: 'E',
            scope: {},
            link: function (scope, element, attrs) {
                scope.toggleLock = function () {
                    scope.locked = !scope.locked;
                    if (scope.locked) {
                        $log.debug('Locking screen to ' + screenOrientation.type);
                        screenOrientation.lock(screenOrientation.type).then(function (result) {
                            console.log(result);
                        }, function (error) {
                            console.log(error);
                        })
                        persistantStorage.setItem('screen-locked-orientation', screenOrientation.type);
                    } else {
                        $log.debug('Unlocking screen');
                        screenOrientation.unlock();
                        persistantStorage.removeItem('screen-locked-orientation')
                    }
                }

                // and locked status
                persistantStorage.getItem('screen-locked-orientation', function (lockedOrientation) {
                    if (lockedOrientation) {
                        screenOrientation.lock(lockedOrientation);
                        scope.locked = true;
                    } else {
                        screenOrientation.unlock();
                        scope.locked = false;
                    }
                });
            }
        }
    } else {
        return {
            template: '',
            restrict: 'E',
            link: function (scope, element, attrs) {
            }
        }
    }
}])


