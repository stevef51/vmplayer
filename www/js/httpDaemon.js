/* This is the Cordova part of things for the index page */
var httpDaemon = (function() {
    // 0 = PRODUCTION (serve from Data directory, allows WWW updates)
    // 1 = DEVELOP (serve from WWW first, no WWW updates but quicker to develop from)
    var SERVE_ORDER = 0;

    var svc = {
        httpd: null,
        url: null,

        start: function (callback) {
            if (!svc.httpd) {
                svc.httpd = (cordova && cordova.plugins && cordova.plugins.CorHttpd) ? cordova.plugins.CorHttpd : null;
            }
            if (svc.httpd) {
                svc.httpd.getURL(function (url) {
                    var startServer = function () {
                        svc.httpd.startServer({
                            'www_root': cordova.file.dataDirectory.substr(7),	// remove 'file://'
                            'port': 45913,
                            'localhost_only': true,
                            'serve_order': SERVE_ORDER
                        }, function (url) {
                            svc.url = url.replace(/\/+$/, '') + '/';

                            console.log('HTTPD running on ' + svc.url);
                            callback(svc.url);
                        }, function (error) {
                            console.error('HTTPD failed ' + error);
                            alert('Unable to start HTTP Server.  Please close and restart the App');
                        });
                    }

                    if (url.length > 0) {
                        svc.url = url.replace(/\/+$/, '') + '/';
                        console.log('HTTPD already running on ' + svc.url + ' stopping and restarting...');
                        svc.httpd.stopServer(startServer, function (error) {
                            callback(svc.url);
                            console.error('HTTPD failed to STOP ' + error);
                        });
                    } else {
                        startServer();
                    }
                })
            }
        },
        onPause: function () {
            console.log('OnPause');
            if (svc.httpd) {
                svc.httpd.stopServer(function () {
                    console.log('HTTPD Stopped');
                }, function (error) {
                    console.error('HTTPD failed to STOP ' + error);
                });
            }
        },
        onResume: function () {
            console.log('OnResume');
            svc.start(function () {
                console.log('HTTPD Restarted');
            });
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app.receivedEvent(...);'
        onDeviceReady: function () {
            document.addEventListener("pause", svc.onPause, false);
            document.addEventListener("resume", svc.onResume, false);
        },
    };

    return svc;
})();